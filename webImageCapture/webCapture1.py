#coding:utf-8

import re
import urllib

'''
利用python抓取网络图片的步骤：
1.根据给定的网址获取网页源代码
2.利用正则表达式把源代码中的图片地址过滤出来
3.根据过滤出来的图片地址下载网络图片
'''
 
def getHtml(url):
    page = urllib.urlopen(url)
    html = page.read()
    return html
 
def getImg(html,path):
    reg = r'src="(.+?\.png)" pic_ext'
    imgre = re.compile(reg)
    imglist = imgre.findall(html)
    x = 0
    for imgurl in imglist:
	#urllib.urlretrieve(imgurl,'%s.jpg' % x)
        urllib.urlretrieve(imgurl,path + '%s.png' % x)
        x = x + 1       
    
#html = getHtml("http://tieba.baidu.com/p/2460150866")
html = getHtml("http://msl1.it.kth.se/tng/poster/")
getImg(html,'C:\\Apps\\Python\\Workspace\\webImageCapture\\images\\')
