#!/usr/bin/env python3
from OSC import OSCClient, OSCMessage
import OSC

client = OSCClient()
client.connect( ("130.229.185.8", 7110) )

#client.send( OSCMessage("/user/1", [1.0, 2.0, 3.0 ] ) )
#client.send( OSCMessage("/user/2", [2.0, 3.0, 4.0 ] ) )
#client.send( OSCMessage("/user/3", [2.0, 3.0, 3.1 ] ) )
#client.send( OSCMessage("/user/4", [3.2, 3.4, 6.0 ] ) )

#client.send( OSCMessage("/user/1" ) )

message = OSC.OSCMessage() #message = OSC.OSCBundle()
message.setAddress("/user/1")
message.append([1.0, 2.0, 3.0 ])
message.append("|this is just a test|")
client.send(message)

#client.send( OSCMessage("/quit") )

