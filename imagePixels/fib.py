from time import clock

def fib(n):
    if n > 1:
        return fib(n - 1) + fib(n -2)
    return n

t1 = clock()
f = fib(35);
t2 = clock()

print t1
print t2

print t2-t1