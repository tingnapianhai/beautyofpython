__author__ = 'KTH'

from PIL import Image

'''
im1 = Image.new('RGB',(240,338),('yellow'))
im1.save('C:\\Users\\KTH\Desktop\\img_py1.jpg')

im2 = Image.new('RGB',(240,338),(255,255,255))
im2.save('C:\\Users\\KTH\Desktop\\img_py2.jpg')

im3 = Image.new('RGB',(240,338),(0,0,0))
im3.save('C:\\Users\\KTH\Desktop\\img_py3.jpg')

im = Image.open('C:\\Users\\KTH\\Desktop\\L01_Tile.jpg')
r,g,b = im.split()
r.save('C:\\Users\\KTH\Desktop\\img_py_r.jpg')
g.save('C:\\Users\\KTH\Desktop\\img_py_g.jpg')
b.save('C:\\Users\\KTH\Desktop\\img_py_b.jpg')

im4=Image.blend(im,im2,0.5)  #combine two images(im & im2) into one(here is im4)
im4.save('C:\\Users\\KTH\Desktop\\img_py_blend.jpg')
'''

def getImageColorB(pathFrom,pathTo):
    image = Image.open(pathFrom)
    r,g,b = image.split()
    r.save(pathTo)

pathFromList = []
pathToList = []
for i in range(1,10):
    #pathFromList.insert(i-1,'C:\\Apps\\Apache2.2\\htdocs\\mytiles\\oldtiles\\L'+str(i)+'_Tile.jpg')
    #pathToList.insert(i-1,'C:\\Apps\\Apache2.2\\htdocs\\mytiles\\newtiles\\L'+str(i)+'_Tile.jpg')
    getImageColorB('C:\\Apps\\Apache2.2\\htdocs\\mytiles\\oldtiles\\L0'+str(i)+'_Tile.jpg' , 'C:\\Apps\\Apache2.2\\htdocs\\mytiles\\newtiles\\L0'+str(i)+'_Tile.jpg');

for i in range(10,47):
    getImageColorB('C:\\Apps\\Apache2.2\\htdocs\\mytiles\\oldtiles\\L'+str(i)+'_Tile.jpg' , 'C:\\Apps\\Apache2.2\\htdocs\\mytiles\\newtiles\\L'+str(i)+'_Tile.jpg');