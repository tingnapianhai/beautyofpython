# -*- coding: utf-8 -*-
from PIL import ImageTk, Image, ImageChops
a = Image.open('./mm.jpg') # mm.jpg is in the same folder
alist = list(a.getdata()) # pixel

width,height = a.size

for x in range(width):
    for y in range(height):
        z,m,c = a.getpixel((x,y)) # get the image's pixel;  z,m,c,v is red,green,blue,alpha
        #z,m,c,v = alist.[x*width+y]
        z2,m2,c2 = int(z*1.2),int(m*1.2),int(c*1.2)
        if z2<0 or z2>255:
            z2 = z
        if m2<0 or m2>255:
            m2 = m
        if c2<0 or c2>255:
            c2 = c
        a.putpixel((x,y),(z2, m2, c2 ))

a.save('./mm2.jpg')
