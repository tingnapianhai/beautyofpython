'''
Created on Sep 25, 2014

@author: KTH
'''
import threading

def worker():
    """thread worker function"""
    print 'Worker'
    return

threads = []
for i in range(5):
    t = threading.Thread(target=worker)
    threads.append(t)
    t.start()


class MyThread(threading.Thread):
    def run(self):
        print "thread from class inherit"

t = MyThread()
t.start()