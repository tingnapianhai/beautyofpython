condition = 10

#if
print 'if statement: '
if condition>10:
    print '>10'
elif condition == 10:
    print '=10'
else:
    print '<10'

#while
print '\nwhile statement: '
while condition > 1:
    print condition
    condition = condition-1
print condition

print '\n switch statement: '
def f(x):
    return {
        1 : 1,
        2 : 2,
    }[x]
print f(condition);

for x in range(1,10):
    print x;