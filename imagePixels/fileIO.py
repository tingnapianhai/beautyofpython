from __future__ import with_statement
import codecs
from cStringIO import StringIO
from time import time

t = time()

with codecs.open(r'C:\Users\KTH\Desktop\test.txt', 'r', 'utf8') as inFile:
  inFile.seek(3) # skip BOM
  content = inFile.read()
  content = content.encode('sjis')

  outBuf = StringIO()
  for c in content:
    outBuf.write(chr(ord(c) ^ 0xAB))

  with open(r'C:\Users\KTH\Desktop\test2.txt', 'wb') as outFile:
    outFile.write(outBuf.getvalue())

print time() - t
