# -*- coding: utf-8 -*-
'''
This is a upgraded & incomplete version, to be continued, someday...:)
'''

import numpy
import csv

'''
strnum = raw_input("Enter the data number: ")
num = int(strnum)
datalist = list()
for i in range(num):
    str1 = raw_input("Enter No."+str(i+1)+' data: ')
    datalist.append(float(str1))

print "Input completed. Total number: " + str(len(datalist))
for i in range(num):
    print datalist[i]
'''

columnNumber = 0

def getCsvData(path = 'data/data.csv'):
    '''this is the initilized funciton, should run first !!! '''
    with open(path,'rb') as file:
        contents = csv.reader(file)


        for row in contents: # 'for' should be inside 'with'
            columnNumber = len(row)
            break
        #print columnNumber

        '''# BreakingPoing点，此时matrix1为保存的二维list()'''
        matrix1 = list(list()) # BreakingPoing点，此时matrix1为保存的二维list()
        for i in range(columnNumber):
            matrix1.append(list()) # 或者 or matrix1.append([])

        matrix = list()
        for row in contents: # 'for' should be inside 'with'
            matrix.append(row[0])
            for i in range(columnNumber):
                matrix1[i].append(row[i])
        return matrix

def ave(dlist):
    '''average of list'''
    return numpy.average(dlist)

def variance(dlist):
    '''variance of the list'''
    return numpy.var(dlist)

def std(dlist):
    '''standard deviation'''
    return numpy.std(dlist)

def anaData(matrix):
    datalist = list()
    for i in range(len(matrix)):
        datalist.insert(i,float(matrix[i][0]))
    return datalist

dlist = getCsvData('data/data2.csv')
datalist = anaData(dlist)
print "(0) Column No. is: " + str(len(datalist))
print "(1) length is: " + str(len(datalist))
print "(2) average is: " + str(ave(datalist))
print "(3) variance is: " + str(variance(datalist))
print "(4) standard deviation is: " + str(std(datalist))
str1 = raw_input("Press Enter to close")
