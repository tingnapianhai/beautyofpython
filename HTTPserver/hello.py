#!/Apps/Python/Python2.7.5/python

# Import modules for CGI handling 
import cgi, cgitb

# Create instance of FieldStorage 
form = cgi.FieldStorage() 

# Get data from fields
first_name = form.getvalue('first_name')
last_name  = form.getvalue('last_name')

with open("output.txt", "w") as text_file:
    text_file.write("first: {} second {}".format(first_name, last_name))
	#text_file.write("First name: %s Last name: %s" % (first_name, last_name))

print "Content-type:text/html\r\n\r\n"
print "<html>"
print "<head>"
print "<title>Hello - Second CGI Program</title>"
print "</head>"
print "<body>"
print "<h2>Hello %s %s</h2>" % (first_name, last_name)
print "</body>"
print "</html>"
