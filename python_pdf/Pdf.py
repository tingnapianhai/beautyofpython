# this is used to separate the pdf files to single-pdf-page

import os
from pyPdf import PdfFileWriter, PdfFileReader

inputpdf = PdfFileReader(open("AndroidMonkeyRunner.pdf", "rb"))

def checkDirectory():
    path = os.getcwd() #get current directory path
    if( os.path.isdir(path+'/pdffiles')==False ):
        os.mkdir('pdffiles')

for i in xrange(inputpdf.numPages):
    output = PdfFileWriter()
    output.addPage(inputpdf.getPage(i))
    checkDirectory() # to make sure the 'pdffiles' exists
    with open(os.getcwd()+"/pdffiles/document-page%s.pdf" % i, "wb") as outputStream:
        output.write(outputStream)
