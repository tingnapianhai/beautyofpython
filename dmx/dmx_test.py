import pysimpledmx
mydmx = pysimpledmx.DMXConnection(32)

mydmx.setChannel(1, 255) # set DMX channel 1 to full
mydmx.setChannel(2, 255) # set DMX channel 2 to 128
mydmx.setChannel(3, 255)   # set DMX channel 3 to 0
mydmx.setChannel(4, 255)   # set DMX channel 3 to 0
mydmx.setChannel(5, 255)
mydmx.render() #render    # render all of the above changes onto the DMX network

#mydmx.setChannel(4, 0, autorender=True) # set channel 4 to full and render to the network